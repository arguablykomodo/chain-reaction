interface CellDiff {
  player: number;
  count: number;
}

interface Diff {
  playing: boolean[];
  counts: number[];
  cells: { [coords: string]: CellDiff };
}

export { Diff };
