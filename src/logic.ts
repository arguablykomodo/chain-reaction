import {
  renderCell,
  renderExplosion,
  renderGameOver,
  renderNextTurn
} from "./render";
import { Diff } from "./save";

interface Player {
  color: string;
  count: number;
  playing: boolean;
}

interface Cell {
  player?: Player;
  count: number;
  threshold: number;
}

interface Game {
  players: Player[];
  w: number;
  h: number;
  grid: Cell[][];
  playerI: number;
  turn: number;
  over: boolean;
  animating: boolean;
  history: Diff[];
}

function get(g: Game, x: number, y: number) {
  return g.grid[y][x];
}

const colors = ["#f00", "#0f0", "#00f", "#ff0", "#f0f", "#0ff", "#f80", "#fff"];

function newGame(w: number, h: number, players: number) {
  const game: Game = {
    players: [],
    w,
    h,
    grid: [],
    turn: 0,
    playerI: 0,
    over: false,
    animating: false,
    history: []
  };

  // Generate players
  for (let i = 0; i < players && i < colors.length; i++) {
    game.players.push({ color: colors[i], count: 0, playing: true });
  }

  // Generate grid
  for (let y = 0; y < h; y++) {
    game.grid.push([]);
    for (let x = 0; x < w; x++) {
      let threshold = 4;
      if (x === 0) threshold--;
      if (x === w - 1) threshold--;
      if (y === 0) threshold--;
      if (y === h - 1) threshold--;
      game.grid[y].push({ count: 0, threshold });
    }
  }
  return game;
}

function updateCell(g: Game, x: number, y: number, p: Player, d: Diff) {
  const c = get(g, x, y);
  const pIndex = g.players.indexOf(p);
  const oldPIndex = g.players.indexOf(c.player!);
  const coords = `${x}x${y}`;
  if (c.count + 1 >= c.threshold) {
    // If its gonna pop
    if (!d.cells[coords])
      d.cells[coords] = {
        count: c.count,
        player: oldPIndex
      };

    c.count = 0;
    c.player!.count--;
    delete c.player;
    renderCell(g, x, y, c);

    return true;
  } else {
    // If we are just adding
    if (!d.cells[coords])
      d.cells[coords] = { count: c.count, player: oldPIndex };

    if (c.player !== p) {
      p.count++;
      if (c.player) {
        c.player.count--;
      }
    }
    c.player = p;
    c.count++;
    renderCell(g, x, y, c);

    return false;
  }
}

type Explosion = [number, number];

async function clickCell(g: Game, x: number, y: number) {
  if (g.over || g.animating) return;

  const p = g.players[g.playerI];
  if (get(g, x, y).player && get(g, x, y).player !== p) return;

  const d: Diff = {
    counts: g.players.map(player => player.count),
    playing: g.players.map(player => player.playing),
    cells: {}
  };

  const result = updateCell(g, x, y, p, d);
  if (result) {
    g.animating = true;
    let queue: Explosion[] = [[x, y]];
    while (queue.length !== 0) {
      // Animate explosions
      const anims: Array<Promise<void[]>> = [];
      for (const e of queue) anims.push(renderExplosion(g, e[0], e[1], p));
      await Promise.all(anims);

      // Update cells
      const newQueue: Explosion[] = [];
      for (const e of queue) {
        if (e[0] !== 0 && updateCell(g, e[0] - 1, e[1], p, d))
          newQueue.push([e[0] - 1, e[1]]);
        if (e[0] !== g.w - 1 && updateCell(g, e[0] + 1, e[1], p, d))
          newQueue.push([e[0] + 1, e[1]]);
        if (e[1] !== 0 && updateCell(g, e[0], e[1] - 1, p, d))
          newQueue.push([e[0], e[1] - 1]);
        if (e[1] !== g.h - 1 && updateCell(g, e[0], e[1] + 1, p, d))
          newQueue.push([e[0], e[1] + 1]);
      }
      queue = newQueue;
    }
    g.animating = false;
  }

  // Remove dead players
  if (g.turn >= g.players.length)
    for (let i = 0; i < g.players.length; i++)
      if (g.players[i].count === 0) {
        g.players[i].playing = false;
        d.playing[i] = false;
      }

  // Check if game is over
  const playersLeft = g.players.filter(pl => pl.playing);
  if (playersLeft.length === 1) {
    g.over = true;
    renderGameOver();
  }

  g.history.push(d);
  g.turn++;

  // Find next player
  g.playerI = (g.playerI + 1) % g.players.length;
  while (!g.players[g.playerI].playing)
    g.playerI = (g.playerI + 1) % g.players.length;

  renderNextTurn(g);
}

const mod = (n: number, m: number) => ((n % m) + m) % m;

function undo(g: Game) {
  if (g.over || g.animating || g.history.length === 0) return;

  const d = g.history.pop()!;
  for (let i = 0; i < d.counts.length; i++) {
    g.players[i].count = d.counts[i];
    g.players[i].playing = d.playing[i];
  }
  for (const coord in d.cells) {
    if (d.cells.hasOwnProperty(coord)) {
      const coords = coord.split("x");
      const diff = d.cells[coord];
      const x = parseInt(coords[0], 10);
      const y = parseInt(coords[1], 10);
      const cell = get(g, x, y);
      cell.count = diff.count;
      cell.player = diff.player === -1 ? undefined : g.players[diff.player];
      renderCell(g, x, y, cell);
    }
  }
  g.turn--;
  g.playerI = mod(g.playerI - 1, g.players.length);
  while (!g.players[g.playerI].playing)
    g.playerI = mod(g.playerI - 1, g.players.length);
  renderNextTurn(g);
}

export { Player, Cell, Game, newGame, clickCell, undo };
