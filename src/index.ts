import { newGame } from "./logic";
import { renderGame } from "./render";

// Install service worker
navigator?.serviceWorker?.register(
  new URL("serviceWorker.ts", import.meta.url),
  { type: "module" },
);

const options = document.getElementById("options") as HTMLDivElement;

const p = document.getElementById("players") as HTMLInputElement;
const w = document.getElementById("width") as HTMLInputElement;
const h = document.getElementById("height") as HTMLInputElement;

const pOut = document.querySelector("output[for=players]") as HTMLOutputElement;
const wOut = document.querySelector("output[for=width]") as HTMLOutputElement;
const hOut = document.querySelector("output[for=height]") as HTMLOutputElement;

pOut.textContent = p.value;
wOut.textContent = w.value;
hOut.textContent = h.value;

p.addEventListener("input", () => (pOut.textContent = p.value));
w.addEventListener("input", () => (wOut.textContent = w.value));
h.addEventListener("input", () => (hOut.textContent = h.value));

document.getElementById("tab")!.addEventListener("click", () => {
  options.classList.toggle("open");
});

document.getElementById("start")!.addEventListener("click", () => {
  options.className = "";
  renderGame(
    newGame(parseInt(w.value, 10), parseInt(h.value, 10), parseInt(p.value, 10))
  );
});

renderGame(
  newGame(parseInt(w.value, 10), parseInt(h.value, 10), parseInt(p.value, 10))
);
